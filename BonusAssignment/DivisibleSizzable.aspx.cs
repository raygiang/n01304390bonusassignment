﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class DivisibleSizzable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void IsPrime(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            bool isPrime = true;
            List<int> divisors = new List<int>();
            var resultString = "";

            int myNum = int.Parse(enteredNum.Text);

            /*  Tests all numbers lower than the given number to find out if the given number
                is prime, also assembles a list of divisors */
            if (myNum == 1)
            {
                isPrime = false;
            }
            else
            {
                for(int i=myNum-1; i>1; i--)
                {
                    if(myNum % i == 0)
                    {
                        divisors.Add(i);
                        isPrime = false;
                    }
                }
            }

            // Print the result and list of divisors if necessary
            if(isPrime == true)
            {
                resultString += "Your number is a prime number!";
            }
            else
            {
                resultString += "Your number is not a prime number! <br />";
                if (myNum != 1)
                {
                    divisors.Reverse();
                    resultString += "Your number is divisible by: " + String.Join(", ", divisors);
                }
            }

            resultArea.InnerHtml = resultString;
        }
    }
}