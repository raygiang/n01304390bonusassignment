﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class CartesianSmartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void FindQuadrant(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            int xCoordinate = int.Parse(xAxisValue.Text);
            int yCoordinate = int.Parse(yAxisValue.Text);

            // Logic to find out which quadrant the given coordinate lies in
            if (xCoordinate > 0 && yCoordinate > 0)
            {
                resultArea.InnerHtml = "Your coordinate lies in quadrant I";
            }
            else if (xCoordinate < 0 && yCoordinate > 0)
            {
                resultArea.InnerHtml = "Your coordinate lies in quadrant II";
            }
            else if (xCoordinate < 0 && yCoordinate < 0)
            {
                resultArea.InnerHtml = "Your coordinate lies in quadrant III";
            }
            else
            {
                resultArea.InnerHtml = "Your coordinate lies in quadrant IV";
            }
        }
    }
}