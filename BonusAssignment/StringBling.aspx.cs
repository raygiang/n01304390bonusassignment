﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class StringBling : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckPalindrome(object sender, EventArgs e)
        {
            String myString = enteredText.Text;
            int length;
            bool isPalindrome = true;

            // Convert the entered text to lower case and remove white spaces
            myString = myString.ToLower();
            myString = myString.Replace(" ", string.Empty);
            length = myString.Length;

            // Compares individual characters in the string
            for (int i=0; i<=length/2; i++)
            {
                if (myString[i] != myString[length-(i+1)])
                {
                    isPalindrome = false;
                }
            }

            if (isPalindrome == true)
            {
                resultArea.InnerHtml = "Your entered text is a palindrome!";
            }
            else
            {
                resultArea.InnerHtml = "Your entered text is not a palindrome!";
            }
        }
    }
}