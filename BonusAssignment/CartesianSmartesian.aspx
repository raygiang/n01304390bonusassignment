﻿<%@ Page Title="Cartesian Smartesian" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CartesianSmartesian.aspx.cs" Inherits="BonusAssignment.CartesianSmartesian" %>

<asp:Content ID="CartesianContent" ContentPlaceHolderID="ModuleArea" runat="server">
    <h2>Cartesian Smartesian</h2>

    <p>
        Enter the x value and y value for your coordinate and I will tell you which quadrant your
        coordinate lies in. (Please enter a non-zero integer value)
    </p>

    <asp:label runat="server" AssociatedControlID="xAxisValue">Value for the x-axis: </asp:label>
    <asp:TextBox runat="server" ID="xAxisValue" placeholder="Please enter the x co-ordinate"></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" ID="xAxisValidator" ErrorMessage="Please enter a coordinate" ForeColor="Red" ControlToValidate="xAxisValue"></asp:RequiredFieldValidator>
    <asp:CompareValidator runat="server" ID="xNonZeroIntegerValidator" ControlToValidate="xAxisValue" Type="Integer" ValueToCompare="0" Operator="NotEqual" ForeColor="Red" ErrorMessage="Please enter a non-zero integer"></asp:CompareValidator>
    <br />

    <asp:label runat="server" AssociatedControlID="yAxisValue">Value for the y-axis: </asp:label>
    <asp:TextBox runat="server" ID="yAxisValue" placeholder="Please enter the y co-ordinate"></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" ID="yAxisValidator" ErrorMessage="Please enter a coordinate" ForeColor="Red" ControlToValidate="yAxisValue"></asp:RequiredFieldValidator>
    <asp:CompareValidator runat="server" ID="yNonZeroIntegerValidator" ControlToValidate="yAxisValue" Type="Integer" ValueToCompare="0" Operator="NotEqual" ForeColor="Red" ErrorMessage="Please enter a non-zero integer"></asp:CompareValidator>
    <br />

    <br />
        <asp:Button runat="server" ID="submitButton" OnClick="FindQuadrant" Text="Submit"/>
    <br />

    <br />
    <div id="resultArea" runat="server">

    </div>
    <br />
</asp:Content>