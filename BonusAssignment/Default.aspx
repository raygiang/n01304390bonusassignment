﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BonusAssignment._Default" %>

<asp:Content ID="SiteIntro" ContentPlaceHolderID="ModuleArea" runat="server">
    <h1>Bonus Assignment</h1>

    <h2>Please select a module:</h2>

    <div class="img-row">
        <div class="col-md-4 col-xm-12">
            <asp:hyperlink id="cartesianSmartesianLink" runat="server" NavigateUrl="~/CartesianSmartesian">
                <asp:image id="cartesianSmartesianImg" runat="server" imageurl="/images/CartesianSmartesian.png" AlternateText="A picture of the cartesian plane" />
            </asp:hyperlink>
        </div>

        <div class="col-md-4 col-xm-12">
            <asp:hyperlink id="divisibleSizzableLink" runat="server" NavigateUrl="~/DivisibleSizzable">
                <asp:image id="divisibleSizzableImg" runat="server" imageurl="/images/DivisibleSizzable.png" AlternateText="A picture of some prime numbers" />
            </asp:hyperlink>
        </div>

        <div class="col-md-4 col-xm-12">
            <asp:hyperlink id="stringBlingLink" runat="server" NavigateUrl="~/StringBling">
                <asp:image id="stringBlingImg" runat="server" imageurl="/images/StringBling.png" AlternateText="A picture of a couple of palindromes" />
            </asp:hyperlink>
        </div>
    </div>
</asp:Content>
