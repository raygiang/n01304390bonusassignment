﻿<%@ Page Title="String Bling" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StringBling.aspx.cs" Inherits="BonusAssignment.StringBling" %>

<asp:Content ID="StringBlingContent" ContentPlaceHolderID="ModuleArea" runat="server">
    <h2>String Bling</h2>

    <p>
        Enter some text and I will tell you if you have a palindrome.
        (Blank spaces will not count)
    </p>

    <asp:label runat="server" AssociatedControlID="enteredText">What would you like to check?: </asp:label>
    <asp:TextBox runat="server" ID="enteredText" placeholder="Please enter some text"></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" ID="enteredTextValidator" ErrorMessage="Please enter some text" ForeColor="Red" ControlToValidate="enteredText"></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator runat="server" ID="regexEnteredText" ValidationExpression="^[a-zA-Z\s]+$" ControlToValidate="enteredText" ErrorMessage="Please use letter characters" ForeColor="Red"></asp:RegularExpressionValidator>
    <%// Regular Expression is from Assignment Instructions %>
    <br />

    <br />
        <asp:Button runat="server" ID="submitButton" OnClick="CheckPalindrome" Text="Is this a palindrome?"/>
    <br />

    <br />
    <div id="resultArea" runat="server">

    </div>
    <br />
</asp:Content>
