﻿<%@ Page Title="Divisible Sizzable" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DivisibleSizzable.aspx.cs" Inherits="BonusAssignment.DivisibleSizzable" %>

<asp:Content ID="DivisibleContent" ContentPlaceHolderID="ModuleArea" runat="server">
    <h2>Divisible Sizzable</h2>

    <p>
        Enter a number and I will tell you if your number is a prime number.
        (Please enter a positive integer)
    </p>

    <asp:label runat="server" AssociatedControlID="enteredNum">What number do you want to check: </asp:label>
    <asp:TextBox runat="server" ID="enteredNum" placeholder="Please enter a positive integer"></asp:TextBox>
    <asp:RequiredFieldValidator runat="server" ID="enteredNumValidator" ErrorMessage="Please enter a number" ForeColor="Red" ControlToValidate="enteredNum"></asp:RequiredFieldValidator>
    <asp:CompareValidator runat="server" ID="positiveIntegerValidator" ControlToValidate="enteredNum" Type="Integer" ValueToCompare="0" Operator="GreaterThan" ForeColor="Red" ErrorMessage="Please enter a positive integer"></asp:CompareValidator>
    <br />

    <br />
        <asp:Button runat="server" ID="submitButton" OnClick="IsPrime" Text="Is my number prime?"/>
    <br />

    <br />
    <div id="resultArea" runat="server">

    </div>
    <br />
</asp:Content>
